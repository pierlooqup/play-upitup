var React = require("react");
var Qajax = require("qajax");
var config = require("../config");
var _ = require("underscore");

var PlaylistTrack = require("./PlaylistTrack");
var SelectFX = require("./SelectFX");

var Player = React.createClass({

    wavesurfer: Object.create(WaveSurfer),

    getDefaultProps: function () {
        return {
            enableAutoSkipOnError: true
        }
    },

    getInitialState: function () {
        return {
            playlist: null,
            playlists: [],
            playIndex: null,
            currentTrack: null,
            playing: false,
            loading: false,
            autoPlay: false,
            loadingPercent: 0
        }
    },

    enableAutoPlay: function () {
        this.wavesurfer.un('ready');
        /*
         * Won't work on iOS until you touch the page :(
         */
        this.wavesurfer.on('ready', () => {
            if (config.environment === "development") console.log("WaveSurfer ready");
            this.wavesurfer.play();
            this.setState({loading: false});
        });
    },

    componentDidMount: function() {
        this.wavesurfer.init({
            container: document.querySelector('#waveform'),
            height: 224,
            interact: true,
            normalize: true,
            cursorColor: "#5c5c5c",
            waveColor: "#1a1a1a",
            progressColor: "#3b3b3b"
        });

        this.wavesurfer.on('finish', function() {
            if (config.environment === "development") console.log("WaveSurfer finish");
            this.seekToNextTrack();
        }.bind(this));

        this.wavesurfer.on('ready', () => {
            this.setState({
                loading: false,
                loadingPercent: 0
            });
            this.wavesurfer.play();
            if (config.environment === "development") console.log("WaveSurfer ready");
        });

        this.wavesurfer.on('error', function(e) {
            this.setState({loading: false});
            console.error("WaveSurfer error", e);

            if (this.props.enableAutoSkipOnError) {
                this.seekToNextTrack();
            }
        }.bind(this));

        this.wavesurfer.on('loading', function(percent) {
            this.setState({
                loading: true,
                loadingPercent: percent
            });
        }.bind(this));

        this.wavesurfer.on('play', function() {
            if (!config.allowAudioPlayback){
                this.wavesurfer.setVolume(0);
            }
            if (config.environment === "development") console.log("WaveSurfer play");
            this.setState({playing: true});
        }.bind(this));

        this.wavesurfer.on('pause', function() {
          if (config.environment === "development") console.log("WaveSurfer pause");
            this.setState({playing: false});
        }.bind(this));
    },

    componentWillMount: function () {
        this.loadPlaylistFromJson();
    },


    loadPlaylistFromJson: function() {
        Qajax({
            url: config.JSON_URL
        })
        .then(Qajax.filterSuccess)
        .progress(function (xhrProgressEvent) {
            if (!xhrProgressEvent.lengthComputable) return;
            var percent = parseInt(
                (xhrProgressEvent.loaded / xhrProgressEvent.total) * 100,
                10
            );
            // TODO playlist load percent?
        })
        .then(Qajax.toJSON)
        .then((json) => {
            this.setState({
                playlists: json.playlists,
                playIndex: 0
            }, () => {
                var randomIndex = Math.floor(Math.random()*json.playlists.length);
                this.loadPlaylistById(json.playlists[randomIndex].id);
            });
        })
        .done();
    },

    loadPlaylistById: function (id) {
        var playlist = _.findWhere(this.state.playlists, {id: id});
        if (!playlist) {
            return;
        }
        if (this.state.playlist && this.state.playlist.id != undefined) {
            if (this.state.playlist.id == playlist.id) {
                return;
            }
        }

        // Shuffle & Dev mode
        if (config.environment === "development") {
            var covers = ["cover.png", "cover.jpg", "thedonut2.jpg", "cover-hires.jpg"];
            playlist.tracks = _.shuffle(playlist.tracks).map(function(t) {
                var randomIndex = Math.floor(Math.random() * covers.length);
                t.mp3 = "track.mp3";
                t.full_cover = "/dist/img/" + covers[randomIndex];
                return t;
            });
        } else {
            playlist.tracks = _.shuffle(playlist.tracks);
        }

        this.setState({
            playlist: playlist,
            playIndex: 0,
            currentTrack: playlist.tracks[0]
        }, () => {
            try {
                this.wavesurfer.load(config.BASE_MP3_URL + playlist.tracks[0].mp3);
            } catch (e) {
                console.error(e);
            }
        });
    },

    playPause: function () {
        try {
            this.wavesurfer.playPause();
        } catch (e) {
            console.error(e);
            try {
                var currentTrack = this.state.playlist.tracks[this.state.playIndex];
                this.enableAutoPlay(()=>{this.wavesurfer.play();});
                this.wavesurfer.load(config.BASE_MP3_URL + currentTrack.mp3);
            } catch (e) {
                console.error(e);
            }
        }
    },

    seekToNextTrack: function () {
        if (!this.state.autoPlay) {
            // iOS thinks it will automatically play the first track,
            // but it doesn't. In any case it's better to let the user hit play
            // the first time, and then autoplay from then on.
            this.enableAutoPlay();
            this.setState({autoPlay: false});
        }

        if (this.state.playIndex !== null) {
            var nextPlayIndex = this.state.playIndex + 1;
            var nextTrack = this.state.playlist.tracks[nextPlayIndex];

            if (nextTrack) {
                this.setState({
                    currentTrack: nextTrack,
                    playIndex: nextPlayIndex
                });

                try {
                    this.wavesurfer.load(config.BASE_MP3_URL + nextTrack.mp3);
                } catch (e) {
                    console.error(e);
                }
            }
        }
    },

    seekToPrevTrack: function () {
        if (this.state.playIndex > 0) {
            var playIndex = this.state.playIndex - 1;
            var prevTrack = this.state.playlist.tracks[playIndex];

            if (prevTrack) {
                this.setState({
                    currentTrack: prevTrack,
                    playIndex: playIndex
                });

                try {
                    this.wavesurfer.load(config.BASE_MP3_URL + prevTrack.mp3);
                } catch (e) {
                    console.error(e);
                }
            }
        }
    },

    handleSelectPlaylist: function(e) {
        this.loadPlaylistById(e.target.value);
    },

    render: function () {
        var showSeekNext = false;
        var showSeekPrev = false;

        if (this.state.playlist) {
            var index = this.state.playIndex;
            var total = this.state.playlist.tracks.length - 1;
            showSeekNext = index < total;
            showSeekPrev = index > 0;
        }

        var track = this.state.currentTrack
            ? (
                <PlaylistTrack
                    track={this.state.currentTrack}
                    trackNum={this.state.playIndex + 1}
                    totalTracks={this.state.playlist && this.state.playlist.tracks.length}
                    showSeekNext={showSeekNext}
                    showSeekPrev={showSeekPrev}
                    handleSeekToNextTrack={this.seekToNextTrack}
                    handleSeekToPrevTrack={this.seekToPrevTrack}
                    handlePlayPause={this.playPause}
                    playing={this.state.playing}
                    loading={this.state.loading}
                />
            )
            : null;

        var playlistTitle = this.state.playlist
            ? this.state.playlist.title + " (" + this.state.playlist.tracks.length + " tracks)"
            : null;

        //var playlistTitleRow = playlistTitle
        //    ? <h2>Now Playing</h2>
        //    : null;
        var playlistTitleRow = null;

        var playlistElements = this.state.playlists.map(function(p, i) {
            return (
                <option key={i} value={p.id}>{p.title}</option>
            );
        }.bind(this))

        var progressClasses = (this.state.loadingPercent > 99)
            ? "progress-bar complete"
            : "progress-bar";

        var loadingProgress = this.state.loading && this.state.loadingPercent
            ? <span className={progressClasses} style={{width: this.state.loadingPercent + "%"}} />
            : <span className="progress-bar complete" style={{width: "100%"}} />;

        var selected = this.state.playlist 
            ? this.state.playlist.id 
            : null;

        return (
            <section className="player pure-u-1">
                <SelectFX
                    onChange={this.loadPlaylistById}
                    placeholder="Playlists"
                    selected={selected}
                    options={this.state.playlists} />
                {playlistTitleRow}
                {track}
                <div className="progress-indicator">
                    {loadingProgress}
                </div>
                <div id="waveform" />
            </section>
        );
    }
});

module.exports = Player;
