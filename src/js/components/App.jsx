var React = require("react");

var Router = require("react-router");
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Link = Router.Link;

var Player = require("./Player");

var App = React.createClass({

    render: function() {
        var year = new Date()
          .getFullYear()
          .toString()
          .split("")
          .map((digit, i) => (<span key={i}>{digit}</span>));

        return (
            <article>
                <header>
                    <a href="http://www.upitup.com" target="_blank">
                        <div id="logo">
                            <div className="row">
                                <span className="spacer"></span>
                                <span>U</span>
                                <span>P</span>
                                <span>I</span>
                                <span>T</span>
                                <span>U</span>
                                <span>P</span>
                                <span className="spacer"></span>
                                <span>C</span>
                                <span>O</span>
                                <span>M</span>
                                <span className="spacer"></span>
                            </div>
                            <div className="row">
                                <span className="spacer"></span>
                                <span>R</span>
                                <span>E</span>
                                <span>C</span>
                                <span>O</span>
                                <span>R</span>
                                <span>D</span>
                                <span>S</span>
                                <span className="spacer"></span>
                                {year}
                                <span className="spacer"></span>
                            </div>
                        </div>
                    </a>
                </header>
                <Player />
                <section id="content">
                    <RouteHandler/>
                </section>
            </article>
        );
    }
});

module.exports = App;
