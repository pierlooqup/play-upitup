var React = require("react/addons");
var _ = require("underscore");

var SelectFX = React.createClass({
    getInitialState: function () {
        return {
            selected: this.props.selected,
            toggled: false
        }
    },
    
    componentWillReceiveProps: function (next, prev) {
      if (next.selected !== prev.selected) {
          this.setState({
              selected: next.selected
          });
      }
    },

    getDefaultProps: function () {
        return {
            placeholder: "Please select",
            onChange: function () {},
            options: []
        }
    },

    toggleSelect: function (e) {
        this.setState({
            toggled: !this.state.toggled
        });
    },

    handleSelectOption: function (value, e) {
        this.setState({
            selected: value
        }, this.props.onChange(value));
    },
    
    getSelectedOptionTitle: function () {
        if (this.state.selected === null) {
            return this.props.placeholder;
        }
        var option = _.find(this.props.options, (el) => {
            return el.id === this.state.selected
        });
        if (!option) {
            return this.props.placeholder;
        }
        return option.title;
    },

    render: function () {
        var selectClasses = React.addons.classSet({
            "cs-select cs-skin-border": true,
            "cs-active": this.state.toggled
        });

        var selectedOption = this.getSelectedOptionTitle();

        var options = this.props.options.map((el, i) => {
            return (
                <li key={i}
                    data-value={el.id}
                    onClick={this.handleSelectOption.bind(null, el.id)}>
                    <span>{el.title}</span>
                </li>
            )
        });

        return (
            <div className={selectClasses}
                tabIndex="0"
                title="Please select..."
                onClick={this.toggleSelect}>
                <span className="cs-placeholder">{selectedOption}</span>
                <div className="cs-options">
                    <ul>
                        <li className="cs-placeholder">
                            <span>{this.props.placeholder}</span>
                        </li>
                        {options}
                    </ul>
                </div>
            </div>
        );
    }
});

module.exports = SelectFX;
