var joinClasses = require('react/lib/joinClasses');
var React = require("react");

module.exports = React.createClass({
    dummyImg: null,

    getInitialState: function() {
        return {
            loaded: false
        };
    },

    onImageLoad: function() {
        // Bullshit. Only works on the first hit
        console.log(this.props.src + " loaded");
        if (this.isMounted()) {
            this.setState({
                loaded: true
            });
        }
    },


    updateSrc: function() {
        var imgTag = this.refs.img.getDOMNode();
        //var imgSrc = imgTag.getAttribute('src');
        var imgSrc = this.props.src;
        this.dummyImg= new window.Image();
        this.dummyImg.onload = this.onImageLoad;
        // You may want to rename the component if the <Image> definition
        // overrides window.Image
        this.dummyImg.src = imgSrc;
    },

    componentDidMount: function() {

        this.dummyImg= new window.Image();
        this.dummyImg.onload = this.onImageLoad;
        this.updateSrc();
    },

    componentWillReceiveProps: function(nextProps){
        //this.setState({
            //loaded: this.props.src === nextProps.src
        //});
    },

    componentWillUpdate: function(nextProps, nextState) {
        if (this.props.src !== nextProps.src) {
            console.log("Update image");
            this.updateSrc();
        }
    },

    render: function() {
        var {className, ...props} = this.props;
        var imgClasses = 'image';

        if (this.state.loaded) {
            imgClasses = joinClasses(imgClasses, 'image-loaded');
        }
        return (
            <img ref="img" {...props} className={joinClasses(className, imgClasses)} />
        );
    }

});
