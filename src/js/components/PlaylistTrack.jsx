var React = require("react");
var config = require("../config");
var joinClasses = require('react/lib/joinClasses');

var ImageComponent = require("./ImageComponent");

var PlaylistTrack = React.createClass({

    getInitialState: function () {
        return {
            loading: false
        }
    },

    getDefaultProps: function () {
        return {
            track: {
                cat_url: "#",
                track_num: 0,
                title: "Unknown Title",
                author: "Unknown",
                album: "Unknown album",
                mp3: "#",
                full_cover: "#",
                small_cover: "#"
            },
            loading: true,
            playing: false,
            showSeekNext: true,
            totalTracks: 0
        }
    },

    getCoverImgSrc: function () {
        return config.BASE_URL + this.props.track.full_cover;
    },

    getSmallCoverImgSrc: function () {
        return config.BASE_URL + this.props.track.small_cover;
    },


    render: function () {

        var coverClasses = joinClasses(
            "artwork",
            this.props.playing && !this.props.loading ? "" : ""
        );
        var cover = (
            <ImageComponent
                ref="cover"
                className={coverClasses}
                src={this.getCoverImgSrc()}
                alt="Artwork"/>
        );

        var playPauseClasses = "fa";
        playPauseClasses = joinClasses(
            playPauseClasses,
            this.props.playing && !this.props.loading ? "fa-pause" : "fa-play",
            this.props.loading ? "fa-spinner" : ""
        );

        var playPauseButtonClasses = joinClasses(
            "pure-button button-xlarge",
            this.props.loading && !this.props.playing  ? "rotating" : ""
        );

        var liClassName = this.props.loading && !this.props.playing
            ? "loading" : null;

        var fullBleedStyle = {
            backgroundImage: this.props.track
              ? `url(${this.getSmallCoverImgSrc()})`
              : "#"
        };

        return (
            <div className="current-track">
                {cover}
                <br/>
                <ul className="controls">
                    <li >
                        <button
                            onClick={this.props.handleSeekToPrevTrack}
                            className="pure-button button-xlarge">
                            <i className="fa fa-fast-backward"></i>
                        </button>
                    </li>
                    <li className={liClassName}>
                        <button
                            disabled={this.props.loading}
                            onClick={this.props.handlePlayPause}
                            className={playPauseButtonClasses}>
                            <i className={playPauseClasses}></i>
                        </button>
                    </li>
                    <li >
                        <button
                            onClick={this.props.handleSeekToNextTrack}
                            className="pure-button button-xlarge">
                            <i className="fa fa-fast-forward"></i>
                        </button>
                    </li>
                </ul>
                <div className="info">
                    <p className="title-row">
                        <span className="trackNum">
                            {this.props.trackNum ? this.props.trackNum : this.props.track.track_num}/{this.props.totalTracks}
                        </span>
                        <span className="title">{this.props.track.title}</span>
                    </p>
                    <p className="album-row">
                        <span className="author">{this.props.track.author}</span>
                        <span>&ndash;</span>
                        <span className="album">
                            <a href={this.props.track.cat_url} target="_blank">
                                {this.props.track.album}
                            </a>
                        </span>
                    </p>
                </div>
                <div className="fullbleed-cover" style={fullBleedStyle}></div>
            </div>
        );
    }
});

module.exports = PlaylistTrack;
