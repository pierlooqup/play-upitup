var config = {
    allowVideoPlayback: false,
    environment: "@@ENV" // development OR production
};

if (config.environment === "development") {
    config.BASE_URL = "http://localhost:8888";
    config.BASE_MP3_URL = "http://localhost:8888/dist/mp3/";
    config.JSON_URL = "./upitup.json";
    allowAudioPlayback = false;
} else {
    config.BASE_URL = "http://www.upitup.com";
    config.BASE_MP3_URL = "http://www.upitup.com";
    config.JSON_URL = "http://www.upitup.com/playlists.json";
    config.allowAudioPlayback = true;
}

module.exports = config;
