var gulp = require('gulp');
var gutil = require('gutil');
var less = require('gulp-less');
var webpack = require('webpack');
var path = require("path");
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var replace = require('gulp-replace');

var config = require("./src/js/config");

var webpackConfig = {
    entry: "./src/js/main.jsx",
    output: {
        path: path.resolve("./dist/"),
        filename: "main.js"
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: "jsx-loader?harmony",
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ["", ".jsx", ".js"]
    }
};

// Use webpack to compile jsx into js,
gulp.task("webpack", function (callback) {
    // Extend options with source mapping
    // run webpack
    webpack(webpackConfig, function (err, stats) {
        if (err) {
            throw new gutil.PluginError("webpack", err);
        }
        //gutil.log("[webpack]", stats.toString({colors: true}));
        callback();
    });
});

gulp.task('less', function() {
    return gulp.src('src/css/*.less')
        .pipe(less())
        .pipe(gulp.dest('dist/css'));
});

gulp.task("watch", ["dist"], function () {
    gulp.watch('src/css/*.less', ["less"]);
    gulp.watch("src/js/**/*.?(js|jsx)", ["dist"]);
});

gulp.task('compress', ["less", "webpack"], function() {
    return gulp.src(['dist/js/*.js', "dist/main.js"])
        .pipe(concat('app.js'))
        .pipe(replace("@@ENV", process.env.NODE_ENV))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task("dist", ["compress"], function () {
    console.log("///////////////////////////////");
    console.log("Built: ", process.env.NODE_ENV);
    console.log("///////////////////////////////");
});

gulp.task("default", ["less", "webpack"], function () {
    console.log("///////////////////////////////");
    console.log("// Built: ", process.env.NODE_ENV);
    console.log("///////////////////////////////");
});
