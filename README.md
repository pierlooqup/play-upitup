# play-upitup
A simple playlist player for Upitup Records, written in React.js

Available at [play.upitup.com](http://play.upitup.com)

# Upitup?

Upitup has been releasing free music since 2003.
Based in Rome, Stuttgart, and Liverpool, we initially just wanted to make our sounds accessible to the public. The idea was to create an open platform for sharing ideas and staying/getting in contact with people. We later realized we're running what's considered a netlabel.

Upitup remains completely uninfected by commercial interests - to the contrary, we love to see the music industry trying to catch up with new technologies while suffering from their self-imposed politics. We are proud to be able to supply our music for free.

[www.upitup.com](http://www.upitup.com)
